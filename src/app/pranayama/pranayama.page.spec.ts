import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PranayamaPage } from './pranayama.page';

describe('PranayamaPage', () => {
  let component: PranayamaPage;
  let fixture: ComponentFixture<PranayamaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PranayamaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PranayamaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
