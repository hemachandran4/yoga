import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PranayamaPageRoutingModule } from './pranayama-routing.module';

import { PranayamaPage } from './pranayama.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PranayamaPageRoutingModule
  ],
  declarations: [PranayamaPage]
})
export class PranayamaPageModule {}
