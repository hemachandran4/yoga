import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-pranayama',
  templateUrl: './pranayama.page.html',
  styleUrls: ['./pranayama.page.scss'],
})
export class PranayamaPage implements OnInit {

  public list = [];

  constructor(private data: DataService) { }

  ngOnInit() {
    this.list = this.data.Pranayama;
  }

}
