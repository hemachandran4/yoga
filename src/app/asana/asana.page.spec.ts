import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AsanaPage } from './asana.page';

describe('AsanaPage', () => {
  let component: AsanaPage;
  let fixture: ComponentFixture<AsanaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsanaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AsanaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
