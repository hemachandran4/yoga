import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-asana',
  templateUrl: './asana.page.html',
  styleUrls: ['./asana.page.scss'],
})
export class AsanaPage implements OnInit {

  public asanaList = [];
  
  constructor(private data: DataService) { }

  ngOnInit() {
    this.asanaList = this.data.Asana;
  }

}
