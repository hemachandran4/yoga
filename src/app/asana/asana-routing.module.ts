import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsanaPage } from './asana.page';

const routes: Routes = [
  {
    path: '',
    component: AsanaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsanaPageRoutingModule {}
