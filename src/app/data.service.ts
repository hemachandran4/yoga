import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public Asana = [
    {
      name: 'Standing',
      shortDesc: 'To know more about standing asanas',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    },
    {
      name: 'Sitting',
      shortDesc: 'To know more about sitting asanas',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    },
    {
      name: 'Twist',
      shortDesc: 'To know more about twist asanas',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    },
    {
      name: 'Prone',
      shortDesc: 'To know more about prone asanas',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    },
    {
      name: 'Supine',
      shortDesc: 'To know more about supine asanas',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    }
  ];

  public Pranayama = [
    {
      name: 'Anulom Vilom',
      shortDesc: 'To know more about anulom vilom',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    },
    {
      name: 'Kapalabati',
      shortDesc: 'To know more about kapalabati',
      description: 'Testf sf sfs sf sdfsdf s /n asdfaf',
      imgURL: 'assets/arms.jpg',
      videoURL: 'assets/sample.mp4'
    }
  ];

  constructor() { }

  checking() {
    console.log("working");
    return 1;
  }
}
