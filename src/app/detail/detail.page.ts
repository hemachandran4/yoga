import { Component, OnInit } from '@angular/core';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  public item = {
    name: '',
    shortDesc: '',
    description: '',
    imgURL: '',
    videoURL: ''
  };
  public type;
  public name;

  constructor(private videoPlayer: VideoPlayer, private data: DataService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    switch (this.type) {
      case 'asana':
        this.item = this.data.Asana.filter(asana => asana.name == this.name)[0];
        break;
      case 'pranayama':
        this.item = this.data.Pranayama.filter(item => item.name == this.name)[0];
        break;
    }
    this.data.checking();
    console.log(this.item, this.data);
  }

  showVideo() {
    this.videoPlayer.play(this.item.videoURL).then(() => {
      console.log('video completed');
    }).catch(err => {
      console.log(err);
    });
  }

}
