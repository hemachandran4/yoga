import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullsessionPageRoutingModule } from './fullsession-routing.module';

import { FullsessionPage } from './fullsession.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FullsessionPageRoutingModule
  ],
  declarations: [FullsessionPage]
})
export class FullsessionPageModule {}
