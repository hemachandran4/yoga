import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FullsessionPage } from './fullsession.page';

describe('FullsessionPage', () => {
  let component: FullsessionPage;
  let fixture: ComponentFixture<FullsessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullsessionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FullsessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
