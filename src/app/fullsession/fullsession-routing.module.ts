import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullsessionPage } from './fullsession.page';

const routes: Routes = [
  {
    path: '',
    component: FullsessionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullsessionPageRoutingModule {}
