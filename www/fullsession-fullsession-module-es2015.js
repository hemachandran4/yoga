(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fullsession-fullsession-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/fullsession/fullsession.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/fullsession/fullsession.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" defaultHref=\"\"></ion-back-button>\n    </ion-buttons>\n    <ion-title size=\"large\" class=\"ion-text-center\">Full Session</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button routerLink=\"\">\n        <ion-icon slot=\"end\" name=\"home-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"top-img\">\n    <img src=\"assets/home.png\" height=\"250\" />\n  </div>\n  <ion-list>\n    <ion-item>\n      <div>\n        <p>\n          Donec magna odio, ac nibh et, vestibulum eleifend felis. Donec ex non\n          quam vulputate malesuada in a magna. Praesent massa arcu, id pharetra\n          et, cursus at lectus.\n        </p>\n        <p>More contents....</p>\n      </div>\n    </ion-item>\n    <ion-item>\n      <video height=\"250\" controls>\n        <source src=\"assets/sample.mp4\" type=\"video/mp4\" />\n      </video>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/fullsession/fullsession-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/fullsession/fullsession-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: FullsessionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullsessionPageRoutingModule", function() { return FullsessionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _fullsession_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fullsession.page */ "./src/app/fullsession/fullsession.page.ts");




const routes = [
    {
        path: '',
        component: _fullsession_page__WEBPACK_IMPORTED_MODULE_3__["FullsessionPage"]
    }
];
let FullsessionPageRoutingModule = class FullsessionPageRoutingModule {
};
FullsessionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FullsessionPageRoutingModule);



/***/ }),

/***/ "./src/app/fullsession/fullsession.module.ts":
/*!***************************************************!*\
  !*** ./src/app/fullsession/fullsession.module.ts ***!
  \***************************************************/
/*! exports provided: FullsessionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullsessionPageModule", function() { return FullsessionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _fullsession_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./fullsession-routing.module */ "./src/app/fullsession/fullsession-routing.module.ts");
/* harmony import */ var _fullsession_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fullsession.page */ "./src/app/fullsession/fullsession.page.ts");







let FullsessionPageModule = class FullsessionPageModule {
};
FullsessionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _fullsession_routing_module__WEBPACK_IMPORTED_MODULE_5__["FullsessionPageRoutingModule"]
        ],
        declarations: [_fullsession_page__WEBPACK_IMPORTED_MODULE_6__["FullsessionPage"]]
    })
], FullsessionPageModule);



/***/ }),

/***/ "./src/app/fullsession/fullsession.page.scss":
/*!***************************************************!*\
  !*** ./src/app/fullsession/fullsession.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Z1bGxzZXNzaW9uL2Z1bGxzZXNzaW9uLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/fullsession/fullsession.page.ts":
/*!*************************************************!*\
  !*** ./src/app/fullsession/fullsession.page.ts ***!
  \*************************************************/
/*! exports provided: FullsessionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullsessionPage", function() { return FullsessionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let FullsessionPage = class FullsessionPage {
    constructor() { }
    ngOnInit() {
    }
};
FullsessionPage.ctorParameters = () => [];
FullsessionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-fullsession',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./fullsession.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/fullsession/fullsession.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./fullsession.page.scss */ "./src/app/fullsession/fullsession.page.scss")).default]
    })
], FullsessionPage);



/***/ })

}]);
//# sourceMappingURL=fullsession-fullsession-module-es2015.js.map