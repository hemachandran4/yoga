(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-category-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/category/category.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/category/category.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content [fullscreen]=\"true\">\n  <ion-header collapse=\"condense\">\n    <ion-toolbar>\n      <ion-title size=\"large\" class=\"ion-text-center\">Yoga</ion-title>\n    </ion-toolbar>\n    <!-- <ion-img height=\"100\" src=\"assets/arms.jpg\"></ion-img> -->\n    <!-- <div class=\"top-img\">\n      <img src=\"assets/home.png\" height=\"250\" />\n    </div> -->\n  </ion-header>\n\n  <!-- <app-explore-container name=\"Tab 1 page\"></app-explore-container> -->\n  <ion-content>\n    <div class=\"top-img\">\n      <img src=\"assets/home.png\" height=\"250\" />\n    </div>\n    <ion-list>\n      <!-- <ion-item>\n        <div class=\"pad\">\n          <h2>What is yoga</h2>\n          <p>\n            Donec magna odio, ac nibh\n            et, vestibulum eleifend felis. Donec\n             ex non quam vulputate\n            malesuada in a magna. Praesent massa arcu,\n             id pharetra et, cursus at\n            lectus.\n          </p>\n          <p>More contents...........</p>\n          <p>More contents...........</p>\n          <h2>Full Session</h2>\n          <p>For full session video </p>\n          <ion-button expand=\"block\">A block button</ion-button>\n        </div>\n      </ion-item> -->\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Arms</h2>\n          <p>For healthy arms</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Shoulders</h2>\n          <p>For healthy shoulders</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <ion-avatar slot=\"start\">\n          <img src=\"assets/arms.jpg\" />\n        </ion-avatar>\n        <ion-label>\n          <h2>Legs</h2>\n          <p>For healthy legs</p>\n        </ion-label>\n      </ion-item>\n\n      \n    </ion-list>\n  </ion-content>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/category/category-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/category/category-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: CategoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageRoutingModule", function() { return CategoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _category_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./category.page */ "./src/app/category/category.page.ts");




const routes = [
    {
        path: '',
        component: _category_page__WEBPACK_IMPORTED_MODULE_3__["CategoryPage"]
    }
];
let CategoryPageRoutingModule = class CategoryPageRoutingModule {
};
CategoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CategoryPageRoutingModule);



/***/ }),

/***/ "./src/app/category/category.module.ts":
/*!*********************************************!*\
  !*** ./src/app/category/category.module.ts ***!
  \*********************************************/
/*! exports provided: CategoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPageModule", function() { return CategoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _category_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./category-routing.module */ "./src/app/category/category-routing.module.ts");
/* harmony import */ var _category_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./category.page */ "./src/app/category/category.page.ts");







let CategoryPageModule = class CategoryPageModule {
};
CategoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _category_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoryPageRoutingModule"]
        ],
        declarations: [_category_page__WEBPACK_IMPORTED_MODULE_6__["CategoryPage"]]
    })
], CategoryPageModule);



/***/ }),

/***/ "./src/app/category/category.page.scss":
/*!*********************************************!*\
  !*** ./src/app/category/category.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhdGVnb3J5L2NhdGVnb3J5LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/category/category.page.ts":
/*!*******************************************!*\
  !*** ./src/app/category/category.page.ts ***!
  \*******************************************/
/*! exports provided: CategoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryPage", function() { return CategoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CategoryPage = class CategoryPage {
    constructor() { }
    ngOnInit() {
    }
};
CategoryPage.ctorParameters = () => [];
CategoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./category.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/category/category.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./category.page.scss */ "./src/app/category/category.page.scss")).default]
    })
], CategoryPage);



/***/ })

}]);
//# sourceMappingURL=category-category-module-es2015.js.map