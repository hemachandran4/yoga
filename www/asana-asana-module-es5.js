(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["asana-asana-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/asana/asana.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/asana/asana.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAsanaAsanaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" defaultHref=\"\"></ion-back-button>\n    </ion-buttons>\n    <ion-title size=\"large\" class=\"ion-text-center\">Asana</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button routerLink=\"\">\n        <ion-icon slot=\"end\" name=\"home-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"top-img\">\n    <img src=\"assets/home.png\" height=\"250\" />\n  </div>\n  <ion-list>\n    <ion-item>\n      <div>\n        <h2>Asana</h2>\n        <p>\n          Donec magna odio, ac nibh et, vestibulum eleifend felis. Donec ex non\n          quam vulputate malesuada in a magna. Praesent massa arcu, id pharetra\n          et, cursus at lectus.\n        </p>\n      </div>\n    </ion-item>\n    <ion-item\n      *ngFor=\"let item of asanaList\"\n      routerLink=\"/detail/asana/{{item.name}}\"\n      detail=\"false\"\n    >\n      <ion-label>\n        <h2>{{item.name}}</h2>\n        <p>{{item.shortDesc}}</p>\n      </ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-redo-outline\"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/asana/asana-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/asana/asana-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: AsanaPageRoutingModule */

    /***/
    function srcAppAsanaAsanaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AsanaPageRoutingModule", function () {
        return AsanaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _asana_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./asana.page */
      "./src/app/asana/asana.page.ts");

      var routes = [{
        path: '',
        component: _asana_page__WEBPACK_IMPORTED_MODULE_3__["AsanaPage"]
      }];

      var AsanaPageRoutingModule = function AsanaPageRoutingModule() {
        _classCallCheck(this, AsanaPageRoutingModule);
      };

      AsanaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AsanaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/asana/asana.module.ts":
    /*!***************************************!*\
      !*** ./src/app/asana/asana.module.ts ***!
      \***************************************/

    /*! exports provided: AsanaPageModule */

    /***/
    function srcAppAsanaAsanaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AsanaPageModule", function () {
        return AsanaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _asana_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./asana-routing.module */
      "./src/app/asana/asana-routing.module.ts");
      /* harmony import */


      var _asana_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./asana.page */
      "./src/app/asana/asana.page.ts");

      var AsanaPageModule = function AsanaPageModule() {
        _classCallCheck(this, AsanaPageModule);
      };

      AsanaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _asana_routing_module__WEBPACK_IMPORTED_MODULE_5__["AsanaPageRoutingModule"]],
        declarations: [_asana_page__WEBPACK_IMPORTED_MODULE_6__["AsanaPage"]]
      })], AsanaPageModule);
      /***/
    },

    /***/
    "./src/app/asana/asana.page.scss":
    /*!***************************************!*\
      !*** ./src/app/asana/asana.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppAsanaAsanaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FzYW5hL2FzYW5hLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/asana/asana.page.ts":
    /*!*************************************!*\
      !*** ./src/app/asana/asana.page.ts ***!
      \*************************************/

    /*! exports provided: AsanaPage */

    /***/
    function srcAppAsanaAsanaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AsanaPage", function () {
        return AsanaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../data.service */
      "./src/app/data.service.ts");

      var AsanaPage = /*#__PURE__*/function () {
        function AsanaPage(data) {
          _classCallCheck(this, AsanaPage);

          this.data = data;
          this.asanaList = [];
        }

        _createClass(AsanaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.asanaList = this.data.Asana;
          }
        }]);

        return AsanaPage;
      }();

      AsanaPage.ctorParameters = function () {
        return [{
          type: _data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]
        }];
      };

      AsanaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-asana',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./asana.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/asana/asana.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./asana.page.scss */
        "./src/app/asana/asana.page.scss"))["default"]]
      })], AsanaPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=asana-asana-module-es5.js.map