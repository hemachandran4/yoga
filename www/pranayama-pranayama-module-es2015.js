(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pranayama-pranayama-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pranayama/pranayama.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pranayama/pranayama.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" defaultHref=\"\"></ion-back-button>\n    </ion-buttons>\n    <ion-title size=\"large\" class=\"ion-text-center\">Pranayama</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button routerLink=\"\">\n        <ion-icon slot=\"end\" name=\"home-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"top-img\">\n    <img src=\"assets/home.png\" height=\"250\" />\n  </div>\n  <ion-list>\n    <ion-item>\n      <div>\n        <h2>Pranayama</h2>\n        <p>\n          Donec magna odio, ac nibh et, vestibulum eleifend felis. Donec ex non\n          quam vulputate malesuada in a magna. Praesent massa arcu, id pharetra\n          et, cursus at lectus.\n        </p>\n      </div>\n    </ion-item>\n    <ion-item\n      *ngFor=\"let item of list\"\n      routerLink=\"/detail/pranayama/{{item.name}}\"\n      detail=\"false\"\n    >\n      <ion-label>\n        <h2>{{item.name}}</h2>\n        <p>{{item.shortDesc}}</p>\n      </ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-redo-outline\"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pranayama/pranayama-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pranayama/pranayama-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: PranayamaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PranayamaPageRoutingModule", function() { return PranayamaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _pranayama_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pranayama.page */ "./src/app/pranayama/pranayama.page.ts");




const routes = [
    {
        path: '',
        component: _pranayama_page__WEBPACK_IMPORTED_MODULE_3__["PranayamaPage"]
    }
];
let PranayamaPageRoutingModule = class PranayamaPageRoutingModule {
};
PranayamaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PranayamaPageRoutingModule);



/***/ }),

/***/ "./src/app/pranayama/pranayama.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pranayama/pranayama.module.ts ***!
  \***********************************************/
/*! exports provided: PranayamaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PranayamaPageModule", function() { return PranayamaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _pranayama_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pranayama-routing.module */ "./src/app/pranayama/pranayama-routing.module.ts");
/* harmony import */ var _pranayama_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pranayama.page */ "./src/app/pranayama/pranayama.page.ts");







let PranayamaPageModule = class PranayamaPageModule {
};
PranayamaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pranayama_routing_module__WEBPACK_IMPORTED_MODULE_5__["PranayamaPageRoutingModule"]
        ],
        declarations: [_pranayama_page__WEBPACK_IMPORTED_MODULE_6__["PranayamaPage"]]
    })
], PranayamaPageModule);



/***/ }),

/***/ "./src/app/pranayama/pranayama.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pranayama/pranayama.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ByYW5heWFtYS9wcmFuYXlhbWEucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pranayama/pranayama.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pranayama/pranayama.page.ts ***!
  \*********************************************/
/*! exports provided: PranayamaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PranayamaPage", function() { return PranayamaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");



let PranayamaPage = class PranayamaPage {
    constructor(data) {
        this.data = data;
        this.list = [];
    }
    ngOnInit() {
        this.list = this.data.Pranayama;
    }
};
PranayamaPage.ctorParameters = () => [
    { type: _data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"] }
];
PranayamaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pranayama',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./pranayama.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pranayama/pranayama.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./pranayama.page.scss */ "./src/app/pranayama/pranayama.page.scss")).default]
    })
], PranayamaPage);



/***/ })

}]);
//# sourceMappingURL=pranayama-pranayama-module-es2015.js.map