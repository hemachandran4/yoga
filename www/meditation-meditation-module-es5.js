(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["meditation-meditation-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/meditation/meditation.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/meditation/meditation.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppMeditationMeditationPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" defaultHref=\"\"></ion-back-button>\n    </ion-buttons>\n    <ion-title size=\"large\" class=\"ion-text-center\">Meditation</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button routerLink=\"\">\n        <ion-icon slot=\"end\" name=\"home-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"top-img\">\n    <img src=\"assets/home.png\" height=\"250\" />\n  </div>\n  <ion-list>\n    <ion-item>\n      <div>\n        <p>\n          Donec magna odio, ac nibh et, vestibulum eleifend felis. Donec ex non\n          quam vulputate malesuada in a magna. Praesent massa arcu, id pharetra\n          et, cursus at lectus.\n        </p>\n        <p>More contents....</p>\n      </div>\n    </ion-item>\n    <ion-item>\n      <!-- <ion-button expand=\"block\" (click)=\"showVideo()\">Show Video</ion-button> -->\n      <video height=\"250\" controls>\n        <source src=\"assets/sample.mp4\" type=\"video/mp4\" />\n      </video>\n    </ion-item>\n  </ion-list>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/meditation/meditation-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/meditation/meditation-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: MeditationPageRoutingModule */

    /***/
    function srcAppMeditationMeditationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeditationPageRoutingModule", function () {
        return MeditationPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _meditation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./meditation.page */
      "./src/app/meditation/meditation.page.ts");

      var routes = [{
        path: '',
        component: _meditation_page__WEBPACK_IMPORTED_MODULE_3__["MeditationPage"]
      }];

      var MeditationPageRoutingModule = function MeditationPageRoutingModule() {
        _classCallCheck(this, MeditationPageRoutingModule);
      };

      MeditationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MeditationPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/meditation/meditation.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/meditation/meditation.module.ts ***!
      \*************************************************/

    /*! exports provided: MeditationPageModule */

    /***/
    function srcAppMeditationMeditationModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeditationPageModule", function () {
        return MeditationPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _meditation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./meditation-routing.module */
      "./src/app/meditation/meditation-routing.module.ts");
      /* harmony import */


      var _meditation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./meditation.page */
      "./src/app/meditation/meditation.page.ts");

      var MeditationPageModule = function MeditationPageModule() {
        _classCallCheck(this, MeditationPageModule);
      };

      MeditationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _meditation_routing_module__WEBPACK_IMPORTED_MODULE_5__["MeditationPageRoutingModule"]],
        declarations: [_meditation_page__WEBPACK_IMPORTED_MODULE_6__["MeditationPage"]]
      })], MeditationPageModule);
      /***/
    },

    /***/
    "./src/app/meditation/meditation.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/meditation/meditation.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppMeditationMeditationPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lZGl0YXRpb24vbWVkaXRhdGlvbi5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/meditation/meditation.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/meditation/meditation.page.ts ***!
      \***********************************************/

    /*! exports provided: MeditationPage */

    /***/
    function srcAppMeditationMeditationPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeditationPage", function () {
        return MeditationPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var MeditationPage = /*#__PURE__*/function () {
        function MeditationPage() {
          _classCallCheck(this, MeditationPage);
        }

        _createClass(MeditationPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return MeditationPage;
      }();

      MeditationPage.ctorParameters = function () {
        return [];
      };

      MeditationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-meditation',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./meditation.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/meditation/meditation.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./meditation.page.scss */
        "./src/app/meditation/meditation.page.scss"))["default"]]
      })], MeditationPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=meditation-meditation-module-es5.js.map